<?php

require_once('Common.php');

class Transport extends Common {

    public $model;
    public $vehicle_class;
    public $manufacturer;
    public $length;
    public $cost_in_credits;
    public $crew;
    public $passengers;
    public $max_atmosphering_speed;
    public $cargo_capacity;
    public $consumables;
    public $pilots;


    public function __construct($data)
    {
        $this->model = $data['model'];
        $this->manufacturer = $data['manufacturer'];
        $this->length = $data['length'];
        $this->cost_in_credits = $data['cost_in_credits'];
        $this->crew = $data['crew'];
        $this->passengers = $data['passengers'];
        $this->max_atmosphering_speed = $data['max_atmosphering_speed'];
        $this->cargo_capacity = $data['cargo_capacity'];
        $this->consumables = $data['consumables'];
        $this->pilots = $data['pilots'];

        parent::__construct($data);
    }

}