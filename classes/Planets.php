<?php

require_once('Common.php');


class Planets extends Common implements getInterface {

//    public $name;
    public $diameter;
    public $rotation_period;
    public $orbital_period;
    public $gravity;
    public $population;
    public $climate;
    public $terrain;
    public $surface_water;
    public $residents;
//    public $films;
//    public $url;
//    public $created;
//    public $edited;


    public function __construct($data) {

        $this->diameter = $data['diameter'];
        $this->rotation_period = $data['rotation_period'];
        $this->rotation_period = $data['rotation_period'];
        $this->orbital_period = $data['orbital_period'];
        $this->gravity = $data['gravity'];
        $this->population = $data['population'];
        $this->climate = $data['climate'];
        $this->terrain = $data['terrain'];
        $this->surface_water = $data['surface_water'];
        $this->residents = $data['residents'];

        parent::__construct($data);

        $surfaceWater = $this->compareSurfaceWaterTerre();
        print_r($surfaceWater);

//        $fullData = $this->getData(); // return full data in class
//        print_r($fullData);
    }

    public function compareSurfaceWaterTerre ()
    { // compare the surface water with the Terre

        if (intval($this->surface_water) > 71) {
            return "La planète " . $this->name . " est recouvert d'eau à " . $this->surface_water . " % ce qui est plus que la Terre qui est recouverte d'eau à 71 %";
        }else if (intval($this->surface_water === 71)) {
            return "La planète " . $this->name . " est recouvert d'eau à " . $this->surface_water . " % comme la Terre";
        }else {
            return "La planète " . $this->name . " est recouvert d'eau à " . $this->surface_water . " % ce qui est plus faible que la Terre qui est recouverte d'eau à 71 %";
        }

    }


    public function getAttribute($class, $attribute)
    {
        // TODO: Implement getAttribute() method.
    }

    public function getData()
    {
        return $this;
        // TODO: Implement getData() method.
    }
}