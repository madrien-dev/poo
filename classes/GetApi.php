<?php


class GetApi {

    public $url;

    public function __construct($url)
    {
        $this->url = "https://swapi.dev/api/" . $url; // defined url for the request in other API
    }

    function getData()
    { // Request API

        var_dump($this->url);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $dataAPI = curl_exec($curl);
        if ($dataAPI === false) {
            throw new Exception(curl_error($curl), curl_errno($curl));
        }
        curl_close($curl);
        return json_decode($dataAPI, true);
    }

}