<?php

require_once "interfaces/ApiInterface.php";




class ApiInfo implements ApiInterface {



    public $edited;
    public $created;
    public $url;


    public function __construct($data) {
        $this->edited = $data['edited'];
        $this->created = $data['created'];
        $this->setUrl($data['url']);
//        var_dump($this);
    }


    // defined the url for the api
    public function setUrl($url)
    {
        return $this->url = $url;
        // TODO: Implement setUrl() method.
    }
}