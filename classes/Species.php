<?php

require_once('Common.php');

class Species extends Common {

//    public $name;
    public $classification;
    public $designation;
    public $average_height;
    public $average_lifespan;
    public $eye_colors;
    public $hair_colors;
    public $skin_colors;
    public $language;
    public $homeworld;
    public $people;
//    public $films;
//    public $url;
//    public $created;
//    public $edited;

    public function __construct($data)
    {

        $this->classification = $data['classification'];
        $this->designation = $data['designation'];
        $this->average_height = $data['average_height'];
        $this->average_lifespan = $data['average_lifespan'];
        $this->eye_colors = $data['eye_colors'];
        $this->hair_colors = $data['hair_colors'];
        $this->skin_colors = $data['skin_colors'];
        $this->language = $data['language'];
        $this->homeworld = $data['homeworld'];
        $this->people = $data['people'];

        parent::__construct($data);
    }
}