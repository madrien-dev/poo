<?php

require_once('Transport.php');

class Starships extends Transport {

//    public $name;
//    public $model;
    public $starship_class;
//    public $manufacturer;
//    public $cost_in_credits;
//    public $length;
//    public $crew;
//    public $passengers;
//    public $max_atmosphering_speed;
    public $hyperdrive_rating;
    public $MGLT;
//    public $cargo_capacity;
//    public $consumables;
//    public $films;
//    public $pilots;
//    public $url;
//    public $created;
//    public $edited;

    public function __construct($data)
    {

        $this->starship_class = $data['starship_class'];
        $this->hyperdrive_rating = $data['hyperdrive_rating'];
        $this->MGLT = $data['MGLT'];


        parent::__construct($data);
    }

}