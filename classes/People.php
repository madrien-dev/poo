<?php

require_once('Common.php');
require_once "interfaces/getInterface.php";

class People extends Common implements getInterface {

//    public $name;
    public $birth_year;
    public $eye_color;
    public $gender;
    public $hair_color;
    public $height;
    public $mass;
    public $skin_color;
    public $homeworld;
//    public $films;
    public $species;
    public $starships;
    public $vehicles;
//    public $url;
//    public $created;
//    public $edited;


    public function __construct($data)
    {

        $this->birth_year = $data['birth_year'];
        $this->eye_color = $data['eye_color'];
        $this->gender = $data['gender'];
        $this->hair_color = $data['hair_color'];
        $this->height = $data['height'];
        $this->mass = $data['mass'];
        $this->skin_color = $data['skin_color'];
        $this->homeworld = $data['homeworld'];
        $this->species = $data['species'];
        $this->starships = $data['starships'];
        $this->vehicles = $data['vehicles'];


        parent::__construct($data);

        $dataAttribute = $this->getAttribute('People','url');
        print_r($dataAttribute);
    }



    public function getAttribute($class,$attribute) // get the data set in the attribute
    {
        if (property_exists($class,$attribute)) {
            return [$attribute => $this->$attribute];
        }else {
            return 'Attribute unknown';
        }        // TODO: Implement displayAttribute() method.
    }

    public function getData() // get full data set
    {
        // TODO: Implement getData() method.
    }
}