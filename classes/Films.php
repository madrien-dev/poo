<?php

require_once('ApiInfo.php');
require_once "interfaces/getInterface.php";


class Films extends ApiInfo implements getInterface {

    public $title;
    public $episode_id;
    public $opening_crawl;
    public $director;
    public $producer;
    public $release_date;
    public $species;
    public $starships;
    public $vehicles;
    public $characters;
    public $planets;
//    public $url;
//    public $created;
//    public $edited;

    public function __construct($data)
    {

        $this->title = $data['title'];
        $this->episode_id = $data['episode_id'];
        $this->opening_crawl = $data['opening_crawl'];
        $this->director = $data['director'];
        $this->producer = $data['producer'];
        $this->release_date = $data['release_date'];
        $this->species = $data['species'];
        $this->starships = $data['starships'];
        $this->vehicles = $data['vehicles'];
        $this->characters = $data['characters'];
        $this->planets = $data['planets'];

        parent::__construct($data);

        /**
         * to test with another value lines 44, 45 and 49 can be uncommented
         */
        $dataAttribute = $this->getAttribute('Films','title');  // return in array the data corresponding in attribute inform
//        $dataAttribute = $this->getAttribute('Films','url');  // return in array the data corresponding in attribute inform
//        $dataAttribute = $this->getAttribute('Films','planets');  // return in array the data corresponding in attribute inform



        // $dataAttribute = $this->getData(); // return full data in class
        print_r($dataAttribute);
    }

    public function getAttribute($class,$attribute) // get the data set in the attribute
    {
        if (property_exists($class,$attribute)) {
            return [$attribute => $this->$attribute];
        }else {
            return 'Attribute unknown';
        }
        // TODO: Implement getAttribute() method.
    }

    public function getData() // get full data set
    {
        return $this;
        // TODO: Implement getData() method.
    }
}