<?php

require_once('Transport.php');

class Vehicles extends Transport {

//    public $name;
//    public $model;
    public $vehicle_class;
//    public $manufacturer;
//    public $length;
//    public $cost_in_credits;
//    public $crew;
//    public $passengers;
//    public $max_atmosphering_speed;
//    public $cargo_capacity;
//    public $consumables;
//    public $films;
//    public $pilots;
//    public $url;
//    public $created;
//    public $edited;

    public function __construct($data)
    {

        $this->vehicle_class = $data['vehicle_class'];

        parent::__construct($data);

        print_r($this->infoVehicles());

    }

    public function infoVehicles () { // little info on vehicles

        return "Le véhicule " . $this->vehicle_class . " a pour model " . $this->model . " sa taille est de " . $this->length . " m, grâce à celle-ci il a une capacitée de " . $this->cargo_capacity  ;
    }

}