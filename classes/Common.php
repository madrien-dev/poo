<?php

require_once('ApiInfo.php');


class Common extends ApiInfo {

    public $name;
    public $film;

    public function __construct($data)
    {
        $this->name = $data['name'];
        $this->film = $data['films'];
        parent::__construct($data);
    }


}