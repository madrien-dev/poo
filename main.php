<?php


require_once('classes/GetApi.php');
require_once('Factory.php');


/**
 * Init instance and test if the instance is good init with a 'uniqid'
 */
var_dump(main::getInstance());
var_dump(main::getInstance());

class main {

    public static $data;
    public $params;
    public $id;
    public $idUnique;
    private static $_instance = null;

    public function __construct ()
    {

        $this->idUnique = uniqid(); // set id unique for test if the instance is not repeated

        $this->params =  $_GET['search']; // get data in url
        if (isset($_GET['id'])) { // test if in params url there is 'id'
            $this->id =  $_GET['id']; // get data in url
            $api = new GetApi($this->params . "/" . $this->id . "/"); // instance GetApi
        }else {
            $api = new GetApi($this->params . "/"); // instance GetApi
        }

        try {
            self::$data = $api->getData(); // call the function getData in getApi
        } catch (Exception $e) {
            var_dump($e);
        }

        if (!is_null($this->id)) { // if attribute 'id' is null return full instance
            new Factory(self::$data); // instance Factory
        }else {
            $dataFull = [];
            foreach (self::$data['results'] as $datum) {
                array_push($dataFull,  new Factory($datum)); // instance Factory and push in array
            }
            return $dataFull;
        }
    }


    public static function getInstance () { // verify instance et initialize instance if === null
        if (is_null(self::$_instance)) {
            self::$_instance = new main();
        }
        return self::$_instance;
    }

}