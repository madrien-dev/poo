<?php

require_once('classes/People.php');
require_once('classes/Starships.php');
require_once('classes/Vehicles.php');
require_once('classes/Species.php');
require_once('classes/Planets.php');
require_once('classes/Films.php');
require_once('main.php');

class Factory {


    public function __construct($dataInput) // choose the class according to the data received from API
    {

        switch (array_keys($dataInput)) { // test the key for defined the good class with a switch
            case [
                'name','height','mass','hair_color','skin_color','eye_color','birth_year','gender',
                'homeworld','films','species','vehicles','starships','created','edited','url'
            ];
                new People($dataInput); // instance People if the key is good
                break;
            case [
                'title','episode_id','opening_crawl','director','producer','release_date','characters',
                'planets','starships','vehicles','species','created','edited','url'
            ];
                new Films($dataInput); // instance Films if the key is good
                break;
            case [
                'name','model','manufacturer','cost_in_credits','length','max_atmosphering_speed','crew',
                'passengers','cargo_capacity','consumables','hyperdrive_rating','MGLT','starship_class','pilots',
                'films','created','edited','url'];
                new Starships($dataInput); // instance Starships if the key is good
                break;
            case [
                'name','model','manufacturer','cost_in_credits','length','max_atmosphering_speed','crew',
                'passengers','cargo_capacity','consumables','vehicle_class','pilots','films',
                'created','edited','url'
            ];
                new Vehicles($dataInput); // instance Vehicles if the key is good
                break;
            case [
               'name','classification','designation','average_height','skin_colors','hair_colors','eye_colors',
                'average_lifespan','homeworld','language','people','films','created','edited','url'
            ];
                new Species($dataInput); // instance Species if the key is good
                break;
            case [
               'name','rotation_period','orbital_period','diameter','climate','gravity','terrain',
                'surface_water','population','residents','films','created','edited','url'
            ];
                new Planets($dataInput); // instance Planets if the key is good
                break;
        }
    }
}