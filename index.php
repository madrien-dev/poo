<?php

include_once("main.php"); // run main.php at launch


/**
 * for test the application run index.php
 * Set in url two params
 * 'search' and 'id'
 * url → yourUrl/?search=films&id=1
 *
 * for test full class in search =
 *
 * ['people','films','starships','vehicles','species','planets']
 *
 * ⚠⚠⚠ → some id doesn't work with search so you have to try another id
 */